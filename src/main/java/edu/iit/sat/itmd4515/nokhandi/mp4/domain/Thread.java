/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import org.eclipse.persistence.annotations.CascadeOnDelete;

/**
 *
 * @author nokhandiar
 * 
 * This bean is used to store all the threads of our forum. 
 * Each thread knows:
 *  Who created it
 *  Subject/Description
 *  Any comments made, attached to the thread
 */
@Entity
@NamedQueries({
@NamedQuery(name = "Thread.findAll", 
            query = "select u from Thread u"),
@NamedQuery(name = "Thread.exists", 
            query = "select u from Thread u where u.id = :id"),
@NamedQuery(name = "Thread.findById",
            query = "select u from Thread u where u.id = :id"),
})
public class Thread implements Serializable{
    @Id @GeneratedValue
    private Long id;
    
    private String subject;
    private String description;
    private Users creator;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
    @JoinColumn(name = "Admins")
    @CascadeOnDelete
    private List<Admins> adminId;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
    @JoinColumn(name = "Comments")
    @CascadeOnDelete
    private List<ThreadComment> comments;

    public Thread(){
        
    }
    
    public Thread(String subject, String description, Users creator){
        this.subject = subject;
        this.description = description;
        this.creator = creator;
    }
    /**
     *
     * @return
     */
    public Long getId(){
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getSubject() {
        return subject;
    }

    /**
     *
     * @param subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return 
     */
    public List<Admins> getAdmins() {
        return adminId;
    }

    /**
     *
     * @param admins
     */
    public void setAdminIDs(List<Admins> admins) {
        this.adminId = admins;
    }

    /**
     *
     * @return
     */
    public Users getCreatorID() {
        return creator;
    }

    /**
     *
     * @param creator
     */
    public void setCreatorID(Users creator) {
        this.creator = creator;
    }

    /**
     *
     * @return
     */
    public List<ThreadComment> getComments() {
        return comments;
    }

    /**
     *
     * @param comments
     */
    public void setComments(List<ThreadComment> comments) {
        this.comments = comments;
    }
    
    /**
     *
     * @param comment
     */
    public void addComment(ThreadComment comment){
        if (!this.comments.contains(comment)){
            comments.add(comment);
        }
    }
    
    /**
     *
     * @param admin
     */
    public void addAdmin(Admins admin){
        if (!this.adminId.contains(admin)){
            adminId.add(admin);
        }
    }
    
}
