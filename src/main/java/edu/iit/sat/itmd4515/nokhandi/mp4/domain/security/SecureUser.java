/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.domain.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Neil Okhandiar
 */
@Entity
@Table(name = "sec_secureUser")
@NamedQueries({
    @NamedQuery(name = "SecureUser.findAll",
            query = "select u from SecureUser u"),
    @NamedQuery(name = "SecureUser.exists",
            query = "select u from SecureUser u where u.username = :uname AND u.password = :pass"),
    @NamedQuery(name = "SecureUser.findByUsername",
            query = "select u from SecureUser u where u.username = :username")})

public class SecureUser implements Serializable {

    @Id
    private String username;
    private String password;
    private boolean enabled;

    @ManyToMany
    @JoinTable(
            name = "sec_admin_groups",
            joinColumns = @JoinColumn(name = "USERNAME"),
            inverseJoinColumns = @JoinColumn(name = "GROUPNAME"))
    private List<SecureUserGroups> groups = new ArrayList<>();

    public SecureUser() {
    }

    public SecureUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void addGroup(SecureUserGroups g) {
        if (!this.groups.contains(g)) {
            this.groups.add(g);
        }
        if (!g.getUser().contains(this)) {
            g.getUser().add(this);
        }
    }

    public List<SecureUserGroups> getGroups() {
        return groups;
    }

    public void setGroups(List<SecureUserGroups> groups) {
        this.groups = groups;
    }

    /**
     * Get the value of password
     *
     * @return the value of password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the value of password
     *
     * @param password new value of password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @PrePersist
    @PreUpdate
    private void hashPassword() {
        String digestPassword = DigestUtils.md5Hex(this.password);
        this.password = digestPassword;
    }

    /**
     * Get the value of username
     *
     * @return the value of username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the value of username
     *
     * @param username new value of username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the value of enabled
     *
     * @return the value of enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set the value of enabled
     *
     * @param enabled new value of enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
