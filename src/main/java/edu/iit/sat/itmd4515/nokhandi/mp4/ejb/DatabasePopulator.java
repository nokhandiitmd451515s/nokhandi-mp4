/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.ejb;

import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Admins;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.ThreadComment;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Thread;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Users;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUserGroups;
//import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.AdminService;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.SecureUserService;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.ThreadCommentService;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.ThreadService;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.UsersService;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Neil Okhandiar
 */
@Startup
@Singleton
public class DatabasePopulator {

    @EJB
    private UsersService userService;

    @EJB
    private ThreadService threadService;

    @EJB
    private AdminService adminService;

    @EJB
    private ThreadCommentService threadCommentService;
    
    @EJB
    private SecureUserService superAdminService;

    @PersistenceContext(unitName = "nokhandiPU")
    private EntityManager em;

    public DatabasePopulator() {

    }

    //Just adds some sample data to the DB, to test with.
    @PostConstruct
    private void seedDatabase() {

        SecureUser neila = new SecureUser("neil", "itmd4515");
        SecureUser priyankaa = new SecureUser("priyanka", "itmd4515");
        SecureUser administrator = new SecureUser("administrator", "itmd4515");
        
        SecureUserGroups adminis = new SecureUserGroups("ADMINS", "a");
        SecureUserGroups useris = new SecureUserGroups("USERS", "b");
        neila.addGroup(useris);
        priyankaa.addGroup(useris);
        administrator.addGroup(adminis);
        
        Users neil = new Users("neil", "itmd4515");
        neil.setUsername("neil");
        neil.setPassword("itmd4515");
        neil.setUser(neila);
        
        
        Users priyanka = new Users("priyanka", "itmd4515");
        priyanka.setUsername("priyanka");
        priyanka.setUsername("itmd4515");
        priyanka.setUser(priyankaa);
        
        
        Admins adminNeil = new Admins();
        adminNeil.setUsername("administrator");
        adminNeil.setPassword("itmd4515");
        adminNeil.setUser(administrator);
        
        em.persist(adminis);
        em.persist(useris);
        superAdminService.create(neila);
        superAdminService.create(priyankaa);
        superAdminService.create(administrator);
        userService.create(neil);
        userService.create(priyanka);
        adminService.create(adminNeil);
        System.out.println("Created neil itmd4515");
    }
}
