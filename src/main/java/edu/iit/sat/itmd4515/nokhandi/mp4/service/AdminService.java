/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.service;

import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Admins;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class AdminService extends AbstractService<Admins> {

    public AdminService() {
        super(Admins.class);
    }

    @Override
    public List<Admins> findAll() {
        return getEntityManager().createNamedQuery("Admins.findAll", Admins.class).getResultList();
    }

    public Admins find(int id) {
        return getEntityManager().createNamedQuery("Admins.findById",
                Admins.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Admins find(String username) {
        return getEntityManager().createNamedQuery("Admins.findByUsername",
                Admins.class)
                .setParameter("username", username)
                .getSingleResult();
    }
}
