/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.service;

//import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Users;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class UsersService extends AbstractService<Users>{
 
    public UsersService() {
        super(Users.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Users> findAll() {
        return getEntityManager().createNamedQuery("Users.findAll",Users.class).getResultList();
    }
    
    public Users find(String username){
        return getEntityManager().createNamedQuery("Users.findByUsername",
                Users.class)
                .setParameter("username", username)
                .getSingleResult();
    }
}
