/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.service;

import edu.iit.sat.itmd4515.nokhandi.mp4.domain.ThreadComment;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class ThreadCommentService extends AbstractService{

    public ThreadCommentService() {
        super(ThreadComment.class);
    }
    
    @Override
    public List<ThreadComment> findAll() {
        return getEntityManager().createNamedQuery("ThreadComment.findAll",ThreadComment.class).getResultList();
    }
    
}
