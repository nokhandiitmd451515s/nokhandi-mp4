/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.web;

import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Admins;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Users;
import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.AdminService;
//import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.SecureUserService;
import edu.iit.sat.itmd4515.nokhandi.mp4.service.UsersService;
//import edu.iit.sat.itmd4515.nokhandi.mp4.service.UsersService;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nokhandi
 */
@WebServlet(name = "RoleTestServlet", urlPatterns = {"/AdminPortal", "/AdminPortal/", "/SuperAdmin/AdminPortal/", "/SuperAdmin/AdminPortal/"})
public class AdminTestPortal extends HttpServlet {

    @EJB
    private SecureUserService adminService;
    @EJB
    private AdminService adminService2;
    @EJB
    private UsersService usersService;
//    @EJB
//    private UsersService userService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * 
     * Page simply states what role you're in, your username and your (hashed) password
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RoleTestServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RoleTestServlet at " + request.getContextPath() + "</h1>");

            request.authenticate(response);

            //request.isUserInRole("Users")
            //Same capitilzation format as the class name
            if (request.isUserInRole("Admins")) {
                Admins l = adminService2.find(request.getRemoteUser());
                out.println("<h2>" + l.getUsername() + " " + l.getPassword() + "</h2>");
                out.println("<h3> You've logged in as an Admin! </h3>");
            } else if (request.isUserInRole("Users")) {
                Users l = usersService.find(request.getRemoteUser());
                out.println("<h2>" + l.getUsername() + " " + l.getPassword() + "</h2>");
                out.println("<h3> You've logged in as an User! </h3>");
            } else {
                out.println("You are not logged in as either User or Admin <br> <h1> WHY ARE YOU HERE </h1>");
            }

            out.println("<a href=\"" + request.getContextPath() + "/logout\">Logout</a>");

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
