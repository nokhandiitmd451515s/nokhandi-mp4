/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.domain;

import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author nokhandiar
 * A security role any user should fulfill by default. Password is stored with md5 hashing
 * 
 * Users can:
 *  Create threads/comments
 *  Delete any threads or comments the user has made (but not ones owned by others)
 *  Find threads or comments they have made
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Users.findAll",
            query = "select u from Users u"),
    @NamedQuery(name = "Users.exists",
            query = "select u from Users u where u.username = :uname AND u.password = :pass"),
    @NamedQuery(name = "Users.findById",
            query = "select u from Users u where u.id = :id"),
    @NamedQuery(name = "Users.findByUsername",
            query = "select u from Users u where u.username = :username")})
public class Users implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;

    @OneToOne
    private SecureUser user;

    public SecureUser getUser() {
        return user;
    }

    public void setUser(SecureUser user) {
        this.user = user;
    }

    public Users(){
        
    }
    
    public Users(String username, String password){
        this.username = username;
        this.password = password;
    }
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @PrePersist
    @PreUpdate
    private void hashPassword() {
        String digestPassword = DigestUtils.md5Hex(this.password);
        this.password = digestPassword;
    }

}
