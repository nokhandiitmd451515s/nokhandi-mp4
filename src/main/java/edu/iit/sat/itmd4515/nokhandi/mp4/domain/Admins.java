/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author nokhandiar
 * A security role users can fulfill. Currently shares the same properties as the 
 * User role.
 * 
 * This role is important for the available options when a thread is being displayed
 * Activities intended for Admins only:
 *  Modify/Delete -anyones- thread or comment
 * 
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Admins.findAll",
            query = "select u from Admins u"),
    @NamedQuery(name = "Admins.exists",
            query = "select u from Admins u where u.username = :uname AND u.password = :pass"),
    @NamedQuery(name = "Admins.findById",
            query = "select u from Admins u where u.id = :id"),
    @NamedQuery(name = "Admins.findByUsername",
            query = "select u from Admins u where u.username = :username")})
public class Admins extends Users implements Serializable {

    @ManyToOne
    private Thread thread;

    public Admins(){
        
    }
    
    public Admins(Thread thread) {
        this.thread = thread;
    }

    public Admins(Thread thread, String username, String password) {
        super(username, password);
        this.thread = thread;
    }
    /**
     *
     * @param thread
     */
    
    public Thread getThreads(){
        return thread;
    }
    public void setThread(Thread threads){
        thread = threads;
    }
}