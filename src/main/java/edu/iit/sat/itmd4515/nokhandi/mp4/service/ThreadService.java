/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.mp4.service;

import edu.iit.sat.itmd4515.nokhandi.mp4.domain.Thread;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author nokhandiar
 */
@Stateless
public class ThreadService extends AbstractService<Thread> {

    /**
     *
     */
    public ThreadService() {
        super(Thread.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Thread> findAll() {
        return getEntityManager().createNamedQuery("Thread.findAll",Thread.class).getResultList();
    }
    
    public Thread find(int id){
        return getEntityManager().createNamedQuery("Thread.findById",
                Thread.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
